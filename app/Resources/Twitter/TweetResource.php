<?php

namespace App\Resources\Twitter;

use Twitter;
use Illuminate\Support\Facades\Cache;

class TweetResource
{
    /**
     * Get the retweets of a tweet, and store them in cache
     *
     * @param  string $id
     * @return \Illuminate\Support\Collection
     */
    public static function getRetweets($id)
    {
        $retweets = Cache::remember('rts-' . $id, 120, function () use ($id) {
            return collect(Twitter::getRts($id, ['count' => 100]));
        });

        return $retweets;
    }
}
