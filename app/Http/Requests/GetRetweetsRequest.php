<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class GetRetweetsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Create custom validation messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required'       => 'The url with a tweet id is required.',
            'id.numeric'        => 'The tweet id can only contain numbers',
            'id.digits_between' => 'Invalid length of tweet id',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric|digits_between:3,19',
        ];
    }

    /**
     * Create custom format for the errors from the given Validator instance.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return array
     */
    public function formatErrors(Validator $validator)
    {
        return array_flatten($validator->getMessageBag()->toArray());
    }
}
