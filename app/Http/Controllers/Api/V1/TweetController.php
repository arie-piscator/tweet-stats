<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Resources\Twitter\TweetResource;
use App\Http\Requests\GetRetweetsRequest;

class TweetController extends Controller
{
    /**
     * Get the reach of a tweet by summing the retweeters' followers count
     *
     * @param  \Illuminate\Http\Requests\GetTweetRequest $request
     * @return \Illuminate\Support\Collection
     */
    public function getReach(GetRetweetsRequest $request)
    {
        $retweets = TweetResource::getRetweets($request->id);

        return collect([
            'retweets'      => $retweets,
            'originalTweet' => $retweets->first()->text,
            'followersSum'  => $retweets->sum('user.followers_count'),
        ]);
    }
}
