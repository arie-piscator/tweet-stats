<?php

namespace Tests\Feature;

use Tests\TestCase;

class UserGetsTweetReachTest extends TestCase
{
    /**
     * Test the status
     *
     * @return void
     */
    public function testStatus()
    {
        $this->get('/')->assertStatus(200);
    }

    /**
     * Test the response of the form action
     *
     * @return void
     */
    public function testFormActionResponse()
    {
        $response = $this->json('GET', '/api/v1/tweet/reach', ['id' => 100]);

        $response->assertStatus(200)->assertJson([
            'retweets'      => true,
            'originalTweet' => true,
            'followersSum'  => true,

        ]);
    }

    /**
     * Test the validation of the tweet id
     *
     * @return void
     */
    public function testTweetIdValidator()
    {
        $response = $this->json('GET', '/api/v1/tweet/reach', ['id' => 'ab']);

        $response->assertStatus(422);
    }

    /**
     * Test resource not found response
     *
     * @return void
     */
    public function testTweetNotFound()
    {
        $response = $this->json('GET', '/api/v1/tweet/reach', ['id' => 102]);

        $response->assertStatus(404);
    }
}
